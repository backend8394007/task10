function printHello(){
    console.log('Hello!');
}

const truncate = (text, length) => {
    return text.slice(0, length) + '...';
}

const getHiddenCard = (number, count = 4) => {
    return '*'.repeat(count) + number.slice(12, 16);
}

printHello();
console.log(truncate('hello', 3));
console.log(getHiddenCard('1234567812345678', 6));
console.log(getHiddenCard('1234567812345678'));